<?php

namespace App\Listeners\OAD;

use App\Events\OAD\ModelWithFilesSavedEvent;
use App\Models\OAD\File as FileModel;

class ModelWithFilesSavedListener
{

    /**
     * Handle the event.
     *
     * @param  ModelWithFilesSavedEvent  $event
     * @return void
     */
    public function handle(ModelWithFilesSavedEvent $event)
    {
        $model  = $event->getSaveResponse();
        $values = $event->getSaveData();
        $pk     = $model->getKeyName();
        
        $model->getFieldsByType('file', $event->getFormName())->each(function($field) use ($model,$values,$pk) {

            $files_hashes   = !empty($values[$field['name']]) && is_array($values[$field['name']]) ? $values[$field['name']] : [];
            
            FileModel::whereIn('hash',$files_hashes)->update([
                'attachment_id'    => $model->$pk,
                'attachment_type'  => $model::class,
                'attachment_field' => $field['db_name'],
                'is_saved'         => true 
            ]);

            #unlinking files saved in before, but removed on this save
            $model->files()
                    ->where([
                        'attachment_id'    => $model->$pk,
                        'attachment_type'  => $model::class,
                        'attachment_field' => $field['db_name'],
                        'is_saved'         => true 
                    ])
                    ->whereNotIn('hash', $files_hashes)
                    ->update(['is_saved' => 0]);
        });

    }
}
