<?php

namespace App\Listeners\OAD;
class ModelWithNotesSavedListener
{

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $model  = $event->getSaveResponse();
        $fields = $model->getFieldsByType('notes', $event->getFormName());
        $values = $event->getSaveData();

        foreach ($fields as $key => $field) {
            if (!empty($values[$key])) { //do not store empty notes
                $model->notes_list()->create([
                    'text'              => $values[$key],
                    'assignable_field'  => $key
                ]);
            }
        }
    }
}
