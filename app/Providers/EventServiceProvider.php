<?php

namespace App\Providers;

use App\Events\OAD\ModelWithFilesSavedEvent;
use App\Events\OAD\ModelWithNotesSavedEvent;
use App\Listeners\OAD\ModelWithFilesSavedListener;
use App\Listeners\OAD\ModelWithNotesSavedListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ModelWithNotesSavedEvent::class => [
            ModelWithNotesSavedListener::class
        ],
        ModelWithFilesSavedEvent::class => [
            ModelWithFilesSavedListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
