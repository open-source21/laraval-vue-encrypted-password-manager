<?php

namespace App\Console\Commands;

use App\Models\OAD\File;
use App\Models\OAD\Note;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\_oad_repo\Commands\OADCommands as Synced;
use App\Models\Pswrd;
use App\Models\Tag;
use Faker\Factory as Faker;

class OADCommands extends Synced
{
    
    protected function iseedNav() {
        $this->call('iseed', ['tables' => 'sections,vue_router,sections_users_types','--force' => true]);
    }

    protected function reseed() {
        parent::reseed();
        $this->runFactories();
     }

    protected function runFactories()
    {
        Pswrd::factory()->count(25)->create()->each(function($item) {
           $faker = Faker::create();
           $notesCount = rand(0, 5);
           if ($notesCount > 0) {
               for ($i = 0; $i < $notesCount; $i++) {
                   $user_hash = User::first()->hash;

                   $note = new Note();
                   $note->text = $faker->address;
                   $note->assignable_id = $item->hash;
                   $note->assignable_type = Pswrd::class;
                   $note->assignable_field = 'pswrd_notes';
                   $note->assignable_type_slug = 'pswrd';
                   $note->user_created = $user_hash;

                   $note->save();
               }
           }
        });
        Tag::factory()->count(15)->create();
        $this->info('Factories completed');
    }

}
