<?php

namespace App\Helpers;

use App\Models\Pswrd;
use Nullix\CryptoJsAes\CryptoJsAes;

class Crypt
{

    public static function encrypt(string $string, string $encryption_hash)
    {
        return CryptoJsAes::encrypt($string, $encryption_hash);
    }

    public static function decrypt(string $encrypted_string, string $encryption_hash)
    {
        return CryptoJsAes::decrypt($encrypted_string, $encryption_hash);
    }

    //generate hash usd for front end ecryption
    public static function getPHash(string $front_end_pass)
    {
        return md5($front_end_pass);
    }
}