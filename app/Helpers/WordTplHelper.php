<?php

namespace App\Helpers;


use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings as PhpWordSettings;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class WordTplHelper
{
    protected $file_path;
    protected $file_name;
    protected $tpl_processor;
    protected $storage_path;

    public function __construct()
    {
        $this->file_path = storage_path('app');
    }

    public function setPath(string $path = '') {
        $this->file_path = storage_path($path);
        File::isDirectory($this->file_path) or File::makeDirectory($this->file_path);
        $this->storage_path = $path;
        return $this;
    }

    public function getFullFilePath(string $ext = 'docx') {
        return $this->file_path . '/' . $this->file_name . '.' . $ext;
    }

    public function getFileName(string $ext = 'docx') {
        return $this->file_name . '.' . $ext;
    }

    public function fillWordTemplate(string $template_path, array $args = []) {

        $this->tpl_processor  = new TemplateProcessor( $template_path );

        foreach ($args as $param => $val) {
            $this->tpl_processor->setValue($param, $val);
        }
        return $this;
    }

    public function saveDocxFromTemplate(string $file_name = 'file', $gen_postfix = true) {

        $this->file_name = $gen_postfix ? $file_name . '_' . \Carbon\Carbon::now()->format('Y-m-d-h-i-s') . '_' . rand(100,10000) : $file_name;

        $this->tpl_processor->saveAs( $this->getFullFilePath() );

        return $this;
    }

    public function convertToPdf( bool $deleteDocx = false ) {

        shell_exec('HOME=/tmp && /usr/bin/libreoffice --invisible --convert-to pdf ' . $this->getFullFilePath('docx') . ' --outdir ' . $this->file_path );
        
        if ($deleteDocx) 
            unlink ($this->getFullFilePath('docx'));

        return $this;

    }

    // SAMPLE USE 
    // return (new WordTplHelper)
    //     ->fillWordTemplate( resource_path('doc_templates/client_auth_doc.docx'), [
    //         'first_name'        => $client->first_name,
    //         'last_name'         => $client->last_name,
    //         'date'              => date('j F, Y')
    //     ])
    //     ->setPath( 'app/tmp/' )
    //     ->saveDocxFromTemplate('auth_doc')
    //     ->convertToPdf(true)
    //     ->getFullFilePath('pdf',false);
    
}
