<?php

namespace App\Events\OAD;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Broadcasting\PrivateChannel;

class ModelWithFilesSavedEvent
{
    protected $storeRes;
    protected $storeData;
    protected $form_name;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Model $updateOrCreateResponse, array $valuesArray, string $form_name)
    {
        $this->storeRes     = $updateOrCreateResponse;
        $this->storeData    = $valuesArray;
        $this->form_name    = $form_name;
    }

    public function getSaveResponse() {
        return $this->storeRes;
    }
    public function getSaveData() {
        return $this->storeData;
    }
    public function getFormName() {
        return $this->form_name;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
