<?php
namespace App\Workers;

use App\Models\Tag;
use App\Models\Pswrd;
use App\Helpers\Crypt;
use App\Models\PswrdsTag;
use App\Helpers\TableHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\_oad_repo\Workers\CRUDWorker;
use Illuminate\Database\Eloquent\Model;
use App\Events\OAD\ModelWithNotesSavedEvent;

class PswrdWorker extends CRUDWorker
{

    public static function genTableData(Object $data, Model $model) : array
    {

        return (new TableHelper(new $model))
                ->massAssignRequestData($data->params)
                ->setCustomFilter('tags', function ($q, $tags) {
                    if (empty($tags)) return $q;
                    foreach($tags as $tag) {
                        $q->whereHas('tags_list', function($q_pivot) use($tag) {
                            $q_pivot->where('tags_hash', $tag);
                        });
                    }
                    return $q;
                })
                ->setQueryExtension(function($q) {
                    return $q->orderBy('favorite', 'desc');
                })
                ->prepareQuery()
                // ->ddQuery()
                ->runQuery()
                ->replaceFields([
                    'actions' => [ //action buttons
                        [
                            'action' => 'edit',
                            'text'   => 'View'
                        ],
                        [
                            'action'    => 'copy_username',
                            'text'      => 'Copy Username'
                        ],
                        [
                            'action'    => 'copy_password',
                            'text'      => 'Copy Password'
                        ],
                        [
                            'action'    => 'copy_url',
                            'text'      => 'Copy Link'
                        ],
                        [
                            'action'    => 'open_url',
                            'text'      => 'Open Link'
                        ],
                        [
                            'action'    => 'delete',
                            'text'      => 'Delete'
                        ]
                    ],
                    'created_at' => function($model) {
                        $date = Carbon::parse($model->created_at);
                        return $date->format('Y-m-d');
                    },
                    'updated_at' => function($model) {
                        $date = Carbon::parse($model->updated_at);
                        return $date->format('Y-m-d');
                    },
                ])
                ->getVuetableResponse(false);
    }

    public static function getShowData(Object $data, Model $model) : array
    {
        $model          = $model::findOrMkNew($data->hash);
        $modelsNvalues  = $model->getFieldsValues();
        $modelsNvalues['tags'] = implode(',', $model->tags()->pluck('tags_hash')->toArray());

        if ($data->hash) self::touch($data->hash);

        return [
                'status'    => 'success',
                'hash'      => $data->hash,
                'forms'     => [
                    'main'  => [
                        'fields'    => $model->form_fields['main'],
                        'values'    => $modelsNvalues
                    ]
                ],
                'data'      => [
                    'created'   => $model->created_at,
                    'updated'   => $model->updated_at
                ]
            ];
    }

    public static function store(Object $data, Model $model, string $form_name = 'main')
    {
        $pswdr = $model::findOrMkNew($data->hash);

        //validation
        $pswdr->validateForm($data->forms[$form_name]['values']);

        $formValues = $data->forms[$form_name]['values'];

        //store clients main form
        $pswdr = $pswdr->setPostSaveEvents([
            ModelWithNotesSavedEvent::class
        ])->store([ 'hash' => $data->hash ], $formValues, false);

        //store tags
        $pswdr->tags_list()->delete();
        $tags_list = explode(',', $data->forms[$form_name]['values']['tags']);
        if ($tags_list[0]) {
            $tags = array_map(function ($hash) {
                return [
                    'tags_hash' => $hash
                ];
            },
                $tags_list);
            $pswdr->tags_list()->createMany($tags);
        }

        return [
            'status'    => 'success',
            'res'       => 'Saved',
            'hash'      => $pswdr->hash,
            'main'      => $pswdr->getFieldsValues(),
            'data'      => [
                    'created'   => $pswdr->created_at,
                    'updated'   => $pswdr->updated_at
            ]
        ];


    }

    public static function loadTags(Request $request) {
        return Tag::select(['icon','title', 'hash'])
            ->when($request->tagsSearch,function($q) use ($request) {
                $q->where('title','LIKE','%'.$request->tagsSearch.'%');
            })
            ->when($request->search != '', function($q) use($request) {
                $q->whereHas('pswrds', function($q_pivot) use($request) {
                    $pswrds = Pswrd::where('title', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('username', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('url', 'LIKE', '%' . $request->search . '%')
                        ->pluck('hash')
                        ->toArray();
                   $q_pivot->whereIn('pswrds_hash', $pswrds);
                });
            })
            ->when($request->selectedTags && !empty($request->selectedTags), function($q) use($request) {
                $q->whereHas('pswrds', function($q_pivot) use($request) {
                    $pswrds = Pswrd::whereHas('tags_list', function($q_pwd) use($request) {
                        $q_pwd->whereIn('tags_hash', $request->selectedTags);
                    })
                        ->pluck('hash')
                        ->toArray();
                    $q_pivot->whereIn('pswrds_hash', $pswrds);
                });
            })
            ->orderBy('title')->get();
    }

    public static function addTag($title)
    {
        $tag = new Tag();
        $tag->icon = 'fal fa-globe';
        $tag->title = $title;
        $tag->save();

        return [
            'hash' => $tag->hash,
            'title' => $tag->title,
            'icon' => $tag->icon,
        ];
    }

    public static function editTag(Request $request)
    {
        if ($request->has('hash')) {
            $tag = Tag::find($request->input('hash'));
            $tag->fill($request->all());
            $tag->save();

            return [
                'hash' => $tag->hash,
                'title' => $tag->title,
                'icon' => $tag->icon,
            ];
        }

        return null;
    }

    public static function deleteTag(Request $request)
    {
        if ($request->has('hash')) {
            PswrdsTag::where('tags_hash', $request->input('hash'))->delete();
            $tag = Tag::find($request->input('hash'));
            $tag->delete();

            return [
                'status' => 'success',
                'res' => 'Tag deleted'
            ];
        }

        return [
            'status' => 'error',
            'res' => 'Tag not found'
        ];
    }

    public static function tagsAddBulk(array $tags, array $hashes)
    {
        self::tagsRemoveBulk( $tags,  $hashes);

        foreach ($hashes as $pswrds_hash) {
            Pswrd::find($pswrds_hash)->tags_list()->createMany(
                collect($tags)
                    ->transform(fn($tags_hash) => [ 'tags_hash' => $tags_hash] )
                    ->toArray()
            );
        }

        return [
            'status' => 'success',
            'res' => 'Tags Added'
        ];
    }

    public static function tagsRemoveBulk(array $tags, array $hashes)
    {
        foreach ($hashes as $pswrds_hash) {
            foreach ($tags as $tags_hash) {
                PswrdsTag::where('pswrds_hash',$pswrds_hash)->where('tags_hash',$tags_hash)->delete();
            }
        }

        return [
            'status' => 'success',
            'res' => 'Tags Removed'
        ];
    }

    public static function destroy(Model $model, string $hash)
    {
        $model::find($hash)->tags_list()->delete();
        if ($model::destroy($hash)) {
            return ['status' => 'success', 'res' => 'Record deleted'];
        }
        return ['status' => 'error', 'res' => 'Failed to delete'];
    }

    public static function updatePasswords($old_hash,$new_hash)
    {
        $passwords = Pswrd::all();
        foreach($passwords as $password) {
            $pswrd              = $password->pswrd;
            $cleanPassword      = Crypt::decrypt($pswrd, $old_hash);
            $encrypted          = Crypt::encrypt($cleanPassword, $new_hash);
            $password->pswrd    = $encrypted;
            $password->save();
        }
    }

    public static function touch(string $hash)
    {

        Pswrd::find($hash)->update([
          'times_opened'=> \DB::raw('times_opened + 1'),
          'last_opened' => now()
        ]);
    }

}
