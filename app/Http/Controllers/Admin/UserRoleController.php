<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Helpers\TableHelper;
use Illuminate\Http\Request;
use App\Models\UserRolePermission;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\LayoutController;
use Illuminate\Support\Facades\Auth;

class UserRoleController extends Controller
{

	protected $model = 'App\Models\UserRole';

	public function index(Request $request)
    {
        User::checkAccess('user_roles',['view','full']);
 
        return (new TableHelper(new $this->model))
                ->massAssignRequestData($request->params)
                ->setQueryExtension(function($q) {
                    return $q->list();
                })
                ->prepareQuery()
                ->runQuery()
                ->replaceFields(
                    Auth::user()->hasPermission('super_admin') ? [
                        'actions' => config('project.table.editDelete')
                    ] : []
                )
                ->getVuetableResponse();
    }

	public function show(Request $request)
    {
        User::checkAccess('user_roles',['view','full']);
        User::checkAccess('super_admin', ['true']);

        $model              = $this->model::findOrMkNew($request->hash);
        $modelsNvalues      = $model->getFieldsValues('main','id');
        $display_menu_type  = 'employee';
        if (auth()->user()->roles_id == '1' && $model->id == 4) {
            $display_menu_type = 'lawyer';
        }
		$menu               = new LayoutController();

        return response()->json(
            [
                'status'    => 'success',
                'hash'      => $request->hash,
                'forms'    => [
                    'main'  => [
                        'fields'    => $model->form_fields['main'],
                        'values'    => $modelsNvalues                    
                    ],
                    'tree'  => [
                        'fields' => $menu->sections_tree(null,$display_menu_type),
                        'values' => UserRolePermission::select('id','permission','sections_id')->where('users_roles_id',$request->hash)->get()
                    ]
                ],
            ],
            200
        );
    }

	public function store(Request $request) {

        User::checkAccess('user_roles',['full']);
        User::checkAccess('super_admin', ['true']);

		$model = $this->model::findOrMkNew($request->hash);

        $model->validateForm($request->forms['main']['values'])
                ->savePermissions([ 'id' => $request->hash ], $request->forms );
    }
    
    public function destroy($hash) {

        User::checkAccess('user_roles',['full']);
        
        $role = $this->model::withCount('users')->find($hash);

        if ($role->users_count) {
            return response()->json(['status' => 'error', 'res' => 'This permission is used by ' . $role->users_count . ' users']);
        }

        if ($this->model::destroy($hash)) {
            return response()->json(['status' => 'success', 'res' => 'Record deleted']);
        }

        return response()->json(['status' => 'error', 'res' => 'Failed to delete']);
    }


}
