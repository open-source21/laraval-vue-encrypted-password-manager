<?php

namespace App\Http\Controllers\Admin;

use App\Workers\PswrdWorker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Request;
use App\_oad_repo\Http\Controllers\UserController as Synced;
use App\Helpers\Crypt;

class UserController extends Synced
{
    public function userData() {
        return \User::userData();
    }

    public function employee_list(Request $request) {
        $model = $this->model::employee()->active()->select(['hash','name']);

        //lookup matching pair for value
        if ($request->hash) {
            return $model->whereIn('hash',explode(',',$request->hash))->get()->pluck('name','hash');
        }
        
        //perform search for results
        if ($request->search) {
            $model->where('name','LIKE','%' . $request->search . '%');
        }
        
        return $model->limit(10)->orderBy('name')->get()->pluck('name','hash');
    }

    public function changePassword(Request $request)
    {
       
        $rules = [
            'current_password' => 'required',
            'password' => 'required|confirmed|min:8|regex:/^(?=.*?[A-Za-z])(?=.*?[0-9]).{8,}$/'
        ];
        $validation = Validator::make($request->all(), $rules);
        
        if ($validation->passes()) 
        {            

            if (!Hash::check($request->current_password, auth()->user()->password)) {
                return response()->json(['status' => 'error', 'res'=> 'Current password is incorrect']);
            }

            auth()->user()->password = $request->password;
            auth()->user()->save();

            // Update Passwords encryption
            $oldDBHashedPass    = Crypt::getPHash($request->current_password);
            $newHashedPass      = Crypt::getPHash($request->password);            
            PswrdWorker::updatePasswords($oldDBHashedPass,$newHashedPass);

            return response()->json(['status' => 'success', 'res'=> 'Your password has been updated', 'phash' => $newHashedPass]);
        }
        return response()->json(['status' => 'error', 'res'=> $validation->getMessageBag()->first()]);
    }

    
}
