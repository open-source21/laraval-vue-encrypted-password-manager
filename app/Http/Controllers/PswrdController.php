<?php
namespace App\Http\Controllers;

use App\Models\Pswrd;
use App\Models\Tag;
use App\Workers\PswrdWorker;
use Illuminate\Http\Request;

class PswrdController extends Controller
{

    public function index(Request $request)
    {
        // return PswrdWorker::spass($request, new Pswrd());

        return response()->json(
            PswrdWorker::genTableData(
                $request,
                new Pswrd()
                )
        );
    }

    public function show(Request $request)
    {
        return response()->json(
            PswrdWorker::getShowData(
                $request,
                new Pswrd()
                )
        );
    }

    public function store(Request $request)
    {
        return response()->json(
            PswrdWorker::store(
                $request,
                new Pswrd()
                )
        );
    }

    public function loadTags(Request $request) {
        return response()->json(
            PswrdWorker::loadTags($request)
        );
    }

    public function tags_list(Request $request)
    {
        $model = Tag::select(['hash','title']);

        //lookup matching pair for value
        if ($request->hash) {
            return $model->whereIn('hash',explode(',',$request->hash))->get()->pluck('title','hash');
        }

        //perform search for results
        if ($request->search) {
            $model->where('title','LIKE','%' . $request->search . '%');
        }

        return $model->limit(10)->orderBy('title')->get()->pluck('title','hash');

    }

    public function addTag(Request $request)
    {
        return response()->json(
            PswrdWorker::addTag($request->tag)
        );
    }

    public function editTag(Request $request)
    {
        return response()->json(
            PswrdWorker::editTag($request)
        );
    }

    public function deleteTag(Request $request)
    {
        return response()->json(
            PswrdWorker::deleteTag($request)
        );
    }

    public function tagsAddBulk(Request $request)
    {
        return response()->json(
            PswrdWorker::tagsAddBulk(explode(',',$request->tags), $request->hashes)
        );
    }

    public function tagsRemoveBulk(Request $request)
    {
        return response()->json(
            PswrdWorker::tagsRemoveBulk(explode(',',$request->tags), $request->hashes)
        );
    }

    public function destroy($hash)
    {
        \User::checkAccess(get_class($this),'full');

        return response()->json(
            PswrdWorker::destroy(new Pswrd(), $hash)
        );
    }

    public function touch(Request $request)
    {
        PswrdWorker::touch($request->hash);
    }

}
