<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function index(Request $request)
    {

        \User::checkAccess(get_class($this),['view','full']);

        //running builder from the trait
        $response = $this->listBuilder(
            new $this->model, //class
            config('vars.table.editDelete')
            //query extention
            //filters query
            //custom search queries
        );

        return response()->json( $response );
    }

    public function show(Request $request)
    {

        \User::checkAccess(get_class($this),['view','full']);

        $model          = $this->model::findOrMkNew($request->hash);
        $modelsNvalues  = $model->getFieldsValues();

        return response()->json(
            [
                'status'    => 'success',
                'hash'      => $request->hash,
                'forms'    => [
                    'main'  => [
                        'fields'    => $model->form_fields,
                        'values'    => $modelsNvalues
                    ]
                    
                ]
            ], 
            200
        );
    }

    public function store(Request $request) {

        \User::checkAccess(get_class($this),['full']);

        $this->model::findOrMkNew($request->hash)
                        ->validateForm($request->forms['main']['values'])
                        ->store([ 'hash' => $request->hash ], $request->forms['main']['values']);
    }

    public function destroy($hash) {

        \User::checkAccess(get_class($this),'full');

        if ($this->model::destroy($hash)) {
            return response()->json(['status' => 'success', 'res' => 'Record deleted']);
        }

        return response()->json(['status' => 'error', 'res' => 'Failed to delete']);
    }

    public function list(Request $request) {

        $model = $this->model::select(['hash','name']);

        //lookup matching pair for value
        if ($request->hash) {
            return $model->where('hash',$request->hash)->get()->pluck('name','hash');
        }

        //perform search for results
        if ($request->search) {
            $model->where('name','LIKE','%' . $request->search . '%');
        }
        if ($request->clients_id) {
            $model->where('clients_id',$request->clients_id);
        }

        return $model->limit(10)->orderBy('name')->get()->pluck('name','hash');
    }

    public function export() {

        \User::checkAccess(get_class($this),['view','full']);

        $modelName  = explode('\\',$this->model);
        $modelName  = end($modelName);
        $mPath      = 'App\\Exports\\' . $modelName . 'Export';
        $file_name  = $modelName . '-' . date('Y-m-d-h-i-s') . '.xlsx';
        Excel::store(new $mPath(), './tmp/' . $file_name);

        return $file_name;       
    }


}
