<?php

namespace App\Http\Controllers\Common;

use App\_oad_repo\Http\Controllers\NoteController as Synced;
use App\Models\OAD\Note;
use Illuminate\Http\Request;

class NoteController extends Synced
{
    public function list(Request $request)
    {
        $hash = is_array($request->form_hash) && count($request->form_hash) == 0 ? null : $request->form_hash;

        return Note::where([
            ['assignable_id',$hash],
            ['assignable_field',$request->field_name],
            ['assignable_type_slug',$request->slug]
        ])->orderBy('notes.created_at','desc')->get()->transform(function($item) {
            return [
                'hash'          => $item->hash,
                'text'          => $item->text,
                'created_at'    => $item->created_at,
                'user_name'     => $item->name,
                'html_text'     => $item->html_text,
            ];
        });

    }

}
