<?php

namespace App\Http\Controllers\Common;

use Auth, Hash, DB;
use App\Models\User;
use App\Helpers\Crypt;
use App\Models\UsersDevice;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Jobs\DeviceVerificationJob;
use Illuminate\Support\Facades\Cookie;
use App\_oad_repo\Http\Controllers\AuthController as Synced;

class AuthController extends Synced
{

    public function login(Request $request)
    {
        $user = \User::select('id','hash','name','email','password','sys_access', 'last_login')->first();

        if (!$user || !Hash::check($request->password, $user->password)) {

            return response()->json([ 'status' => 'error', 'res' => 'Invalid Credentials' ], 250);

        } else if ($user->sys_access != 1) {

            return response()->json([ 'status' => 'error', 'res' => 'Access Denied' ], 250);

        }
        if (app()->environment() === 'production') {
            $device_token = Cookie::get('device_token');
            if ($device_token && $user->last_login) {
                $device_token = Cookie::get('device_token');
                $device = UsersDevice::where('token', $device_token)
                    ->where('users_id', $user->id)
                    ->where('verified', true)
                    ->first();
                if (!$device) {
                    $code = '';

                    for($i = 0; $i < 6; $i++) {
                        $code .= mt_rand(0, 9);
                    }

                    $user_device = new UsersDevice();
                    $user_device->users_id = $user->id;
                    $user_device->token = $device_token;
                    $user_device->verification_code = $code;
                    $user_device->expire_date = Carbon::now()->addMonth(3);
                    $user_device->save();

                    dispatch(new DeviceVerificationJob([
                        'email' => $user->email,
                        'code' => $user_device->verification_code
                    ]))->delay(Carbon::now());

                    return response()->json([ 'status' => 'warning', 'res' => 'Please verify your email', 'action' => 'verify', 'id' => $user_device->id ], 250);
                }
            }
        }

        //delete all other device sessions for this user
        $user->tokens()->where('tokenable_id',$user->id)->delete();

        auth()->login($user);
        User::where('id', $user->id)->update(['last_login' => Carbon::now()]);
        $session = $user->createToken('device-session');
 
        $phash = Crypt::getPHash($request->password);

        return response()->json([
            'status'        => 'success',
            'res'           => 'Signed In',
            'token'         => $session->plainTextToken,
            'user'          => collect($user)->except(['id','sys_access'])
                                ->merge(['phash' => $phash]),
            'expiration'    => config('sanctum.expiration')
        ],
            200);

    }

}
