<?php

namespace App\Http\Controllers\Common;

use App\Models\OAD\File;
use Carbon\Carbon;
use App\Models\OAD\File as FileModel;
use Illuminate\Http\Request;
use App\_oad_repo\Http\Controllers\FileController as Synced;

class FileController extends Synced
{
    
    public function store(Request $request) {
        $return = [];

        if (count($request->all())) {
            foreach ($request->all() as $file) {

                $model = FileModel::store( $file->getPathName(), '' , [
                    'file_name' => $file->getClientOriginalName(),
                    'ext'       => strtolower($file->getClientOriginalExtension())
                ]);
                   
                $return[] = [
                    'hash'      => $model->hash,
                    'name'      => $model->file_name,
                    'user_name' => $model->user->name,
                    'date'      => Carbon::parse( $model->created_at )->format('d M, Y'),
                    'ext'       => $model->ext,
                    'url'       => '/view_file/' . $model->hash
                ];

            }
        }

        return response()->json($return);
    }

    public function view($hash, $file_name = '') {

        $file = $this->model::find($hash);
        $file_name = $file_name ? $file_name : $file->file_name;

        $headers = [
            'Content-Type'          => $file->mime ,
            'Content-Disposition'   => 'filename="'. $file_name.'"'
        ];

        return response()->file(
            storage_path( $file->path ), $headers
        );
        
    }

    public function get_page_thumb(Request $request) {
        return response( 
                        FileModel::gen_file_page_thumb($request->hash,$request->page ?? 1,$request->size ?? 'small') 
                    )
                    ->header('Content-type','image/png');
    }

    public function list(Request $request)
    {
        $hash = is_array($request->form_hash) && count($request->form_hash) == 0 ? null : $request->form_hash;

        return File::with('user')
            ->where([
                ['attachment_id',$hash],
                ['attachment_field',$request->field_name]
        ])->orderBy('files.created_at','desc')->get()->transform(function($item) {
            return [
                'name'          => $item->file_name,
                'created_at'    => $item->created_at,
                'user_name'     => $item->user->name,
                'hash'     => $item->hash,
            ];
        });
    }
    
}
