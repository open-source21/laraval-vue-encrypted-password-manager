<?php
namespace App\Models;

use Uuid;
use App\Models\OAD\Field;
use App\Models\OAD\OADModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tag extends OADModel
{
    use HasFactory;
    
	protected $table = 'tags';
	protected $guarded = ['hash'];
	protected $primaryKey = 'hash';
    public $incrementing = false;
    protected $fillable = ['icon', 'title'];

	public $form_fields = [];

	public function buildFields($return_form_fields = false) {

		$this->form_fields['main'] = $this->buildFormFields([
             Field::init()->name('icon')->label('Tag')->toArray(),
             Field::init()->name('title')->label('Tag')->toArray(),
         ]);

        return $return_form_fields ? $this->form_fields[$return_form_fields] : $this;
    }

	public function scopeList($query)
    {
        return $query;
    }

    public function scopeExportList($query) {
		return $query->select('hash');
	}

	public static function boot() {
        parent::boot();

        self::creating(function($model) {

			$model->hash = Uuid::generate()->string;

        });

    }

    public function pswrds()
    {
        return $this->hasMany(PswrdsTag::class, 'tags_hash', 'hash');
    }


}
