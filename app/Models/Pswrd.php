<?php
namespace App\Models;

use App\Helpers\Crypt;
use Uuid;
use App\Models\OAD\Field;
use App\Models\OAD\OADModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pswrd extends OADModel
{
    use HasFactory;

	protected $table = 'pswrds';
	protected $guarded = ['hash'];
	protected $primaryKey = 'hash';
    public $incrementing = false;
    protected $casts = [
        'pswrd' => 'encrypted'
    ];

	public $form_fields = [];

	public function buildFields($return_form_fields = false) {

		$this->form_fields['main'] = $this->buildFormFields([
             Field::init()->name('title')->label('Title')->toArray(),
             Field::init()->name('username')->label('Username')->toArray(),
             Field::init()->type('password')->name('pswrd')->label('Password')->toArray(),
             Field::init()->name('url')->label('Url')->toArray(),
             Field::init()->type('notes')->name('pswrd_notes')->label('Notes')->toArray(),
             Field::init()->name('favorite')->toArray(),
             Field::init()->type('select')->name('tags')->ajax('/d/pswrds/tags_list')->multiple(true)->label('Tags')->toArray(),
         ]);

        return $return_form_fields ? $this->form_fields[$return_form_fields] : $this;
    }

	public function scopeList($query)
    {
        return $query;
    }

    public function scopeExportList($query) {
		return $query->select('hash');
	}

	public static function boot() {
        parent::boot();

        self::creating(function($model) {
			       $model->hash = Uuid::generate()->string;
             if (!isJson($model->pswrd)) {
                  $model->pswrd = Crypt::encrypt($model->pswrd);
              }
              $model->favorite = $model->favorite ?? 0;
              $model->last_opened = now();
        });

        function isJson($string) {
            json_decode($string);
            return json_last_error() === JSON_ERROR_NONE;
        }

    }

    public function tags_list()
    {
        return $this->hasMany(PswrdsTag::class, 'pswrds_hash', 'hash');
    }

    public function tags()
    {
        return $this->belongsToMany(
            Tag::class,
            PswrdsTag::class,
            'pswrds_hash',
            'tags_hash',
            'hash',
            'hash'
        );
    }

}
