<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VueRouter extends Model
{
    use HasFactory;

    protected $table = 'vue_router';
    public $timestamps = false;
}
