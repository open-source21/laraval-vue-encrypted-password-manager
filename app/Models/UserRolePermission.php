<?php

namespace App\Models;

use App\_oad_repo\Models\UserRolePermission as Synced;
use App\Models\OAD\Section;

class UserRolePermission extends Synced
{
    public function section()
    {
        return $this->belongsTo(Section::class, 'sections_id');
    }
}
