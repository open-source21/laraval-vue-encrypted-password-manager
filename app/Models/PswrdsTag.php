<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PswrdsTag extends Model
{
    use HasFactory;

    protected $fillable = ['tags_hash'];

    public $timestamps = false;
    public $incrementing = false;
}
