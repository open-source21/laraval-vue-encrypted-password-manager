<?php

namespace App\Models;

use App\_oad_repo\Models\UserRolePermission;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laravel\Sanctum\HasApiTokens;
use App\_oad_repo\Models\User as Synced;

class User extends Synced
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name', 'email', 'password', 'roles_id','sys_access','user_type'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function userData() {
        return [
            'type'      => self::userType(),
            'permissions' => self::get_permissions_by_names()
        ];
    }

    public static function userType() {
        return Auth::user()->user_type;
    }

    public static function get_permissions_by_names()
    {
        return UserRolePermission::with('section')->where('users_roles_id',
            self::find(Auth::user()->id)->roles_id
        )->get()->pluck('permission', 'section.slug')->toArray();
    }

    public static function hasPermission($slug, $permission = 'true')
    {
        return (isset(self::get_permissions_by_names()[$slug]) && self::get_permissions_by_names()[$slug] === $permission);
    }
}
