<?php

namespace App\Models\OAD;

use App\_oad_repo\Models\OADModel as Synced;

class OADModel extends Synced {

    public function scopeJoinClient($query,$join_on_field) {
        $this_table = with(new $this)->getTable();
        $query->leftJoin('clients' , $this_table . '.' . $join_on_field, '=', 'clients.hash');
    }

    public function scopeSelectClientName($q) 
    {
        return $q->addSelect( \DB::raw(" CONCAT_WS(' ',clients.first_name, clients.last_name) as client_name ") );
    }

    public function scopeSelectLawyerName($q) 
    {
        return $q->addSelect( \DB::raw(" CONCAT_WS(' ',users.first_name, users.last_name) as lawyer ") );
    }

    public function scopeSelectLawFirmName($q) 
    {
        return $q->addSelect( \DB::raw(" lawfirms.name as lawfirm ") );
    }

    public function formatPhoneForSaving($val) {
        $val = preg_replace('/[^0-9]/', '', $val);
        return substr($val, -10);
    }

}
