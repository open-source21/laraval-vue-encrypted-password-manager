<?php

namespace App\Models\OAD;

use App\_oad_repo\Models\File as Synced;

class File extends Synced {

    public static function gen_file_page_thumb(string $hash, int $page = 1, string $thumb_size = 'small') {
        
        $file       = self::findOrFail($hash);
        $image      = new \Imagick();
        switch ($thumb_size) {
            case 'small':
                $height = '200';
                $width  = '200';
            break;
            default:
                $height = '1500';
                $width  = '1500';
        }
        $image->setResolution(150, 150);
        $image->readImage(storage_path($file->path));
        $image->setImageBackgroundColor('white');
        $image->setIteratorIndex($page - 1);
        $image->setImageFormat('png');
        $image->scaleImage($height, $width, true);
        $image->setImageAlphaChannel(\Imagick::ALPHACHANNEL_REMOVE);
        // $image->mergeImageLayers(\Imagick::LAYERMETHOD_FLATTEN);
        return $image;
        
    }

}
