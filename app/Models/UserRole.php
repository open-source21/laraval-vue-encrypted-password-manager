<?php

namespace App\Models;

use App\_oad_repo\Models\UserRole as Synced;

class UserRole extends Synced
{
	public function scopeList($query)
    {
        $q = $query->where('id', '>', 1)->where('id', '!=', 4);
        if (auth()->user()->roles_id > 1) $q = $q->where('id','!=',4); //only admin can admin lawyers roles
        return $q;
	}
}
