import Vue from "vue";
import Router from "vue-router";
import CONF from './config';
import { frontAuth, backAuth } from '@/_oad_repo/auth';
import beforeEachRoute from '@/_oad_repo/sys/beforeEachRoute'
import afterEachRoute from '@/_oad_repo/sys/afterEachRoute'
import pathThrough from '@/_oad_repo/components/parentBlankComponent'

Vue.use(Router);

// create new router
const routes = [
    {
        path: "/",
        component: () => import('@/views/frontend/layout'),
        beforeEnter: frontAuth,
        children: [
            {
                path: "/",
                component: () => import('@/views/frontend/auth/login'),
                name: 'login',
                meta: { title: 'Login | ' + CONF.APP_NAME },
            },
            {
                path: "/auth/verify_device",
                name: 'verify_device',
                component: () => import('@/views/frontend/auth/verify_device'),
                meta: { title: 'Verify Device | ' + CONF.APP_NAME },
            },
            {
                path: "/auth/forgot_password",
                name: 'forgot_password',
                component: () => import('@/views/frontend/auth/forgot_password'),
                meta: { title: 'Forgot Password | ' + CONF.APP_NAME },
            },
            {
                path: "/auth/reset_password",
                name: 'reset_password',
                component: () => import('@/views/frontend/auth/reset_password'),
                meta: { title: 'Reset Password | ' + CONF.APP_NAME },
            },
            {
                path: "/auth/reset_password_link/:email/:token",
                name: 'reset_password_link',
                component: () => import('@/views/frontend/auth/reset_password'),
                meta: { title: 'Reset Password | ' + CONF.APP_NAME },
            },
            {
                path: "/auth/reset_password_link/expired",
                name: 'reset_password_link_expired',
                component: () => import('@/views/frontend/auth/reset_password_expired'),
                meta: { title: 'Reset Password | ' + CONF.APP_NAME },
            }
        ]
    },
    {
        path: "/app",
        component: () => import('@/views/backend/common/layout'),
        beforeEnter: backAuth,
        children: [
            {
                path: "/dashboard",
                name: 'dashboard',
                component: () => import('@/views/backend/dashboard/dashboard'),
                meta: { title: 'Dashboard | ' + CONF.APP_NAME },
            },
            {
                path: "/pswrds",
                name: 'pswrds',
                component: () => import('@/views/backend/pswrds/pswrdsIndex'),
                meta: { title: 'Passwords | ' + CONF.APP_NAME },
                children: [               
                    {
                        path: "details/:hash",
                        name: 'pswrdsForm',
                        component: () => import('@/views/backend/pswrds/pswrdsForm'),
                        meta: { title: 'Passwords | ' + CONF.APP_NAME },
                    },                  
                ]
            },
            {
                path: "/admin",
                name: 'admin',
                component: pathThrough,
                children: [                    
                    {
                        path: "users",
                        name: 'users',
                        component: () => import('@/views/backend/users/Users'),
                        meta: { title: 'Employees | ' + CONF.APP_NAME },
                        children: [               
                            {
                                path: "details/:hash",
                                name: 'usersForm',
                                component: () => import('@/views/backend/users/UsersForm'),
                                meta: { title: 'Employee | ' + CONF.APP_NAME },
                            },                  
                        ]
                    },
                    {
                        path: "user_roles",
                        name: 'usersRoles',
                        component: () => import('@/views/backend/user-roles/UserRoles'),
                        meta: { title: 'User Permissions | ' + CONF.APP_NAME },
                        children: [                    
                            {
                                path: "details/:hash",
                                name: 'usersRolesForm',
                                component: () => import('@/views/backend/user-roles/UserRolesForm'),
                                meta: { title: 'User Permissions | ' + CONF.APP_NAME },
                            },                  
                        ]
                    },            
                ]
            },
        ]
    },
    {
        path: "*",
        component: () => import('@/views/pages/notFound'),
        meta: { title: 'Page Not Found | ' + CONF.APP_NAME },
    }
];

const router = new Router({
    mode: "history",
    linkActiveClass: "active",
    routes,
    scrollBehavior(to, from, savedPosition) {
    return {x: 0, y: 0};
}
});

router.beforeEach(beforeEachRoute);
router.afterEach(afterEachRoute);

export default router;
