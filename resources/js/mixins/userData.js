const userData = {
	props: ['data','value','index'],
	data() {
		return {
            haveUserData: false,
			userData: {
                type: '',
                level: ''
            }
		}
	},
    methods: {
        getUserData() {
            axios('/d/users/userData')
            .then((res) => {
								if (res && res.data) {
									this.userData = res.data;
									this.haveUserData = true;
								}
            })
        }
    },    
	computed: {
				hasPermission() {
					return (slug, permission = 'true') => {
						return (this.userData.permissions && this.userData.permissions[slug] === permission)
					}
				}
	},
    mounted() {
        this.getUserData();
    }
}
export default userData
