import CryptoJS from 'crypto-js'

const crypt = {
    data() {
        return {
            config: {
                stringify: (cipherParams) => {
                    let j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
                    if (cipherParams.iv) j.iv = cipherParams.iv.toString();
                    if (cipherParams.salt) j.s = cipherParams.salt.toString();
                    return JSON.stringify(j);
                },
                parse: function (jsonStr) {
                    let j = JSON.parse(jsonStr);
                    let cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
                    if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
                    if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
                    return cipherParams;
                }
            }
        }
    },
    computed: {
        passphrase() {
            return this.$store.state.user.phash
        }
    },
    methods: {
        encrypt(value) {
            return CryptoJS.AES.encrypt(JSON.stringify(value), this.passphrase, {format: this.config}).toString();
        },
        decrypt(value) {
            return JSON.parse(CryptoJS.AES.decrypt(value, this.passphrase, {format: this.config}).toString(CryptoJS.enc.Utf8));
        }
    }
}

export default crypt