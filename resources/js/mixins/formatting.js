import formatting from '@/_oad_repo/mixins/formatting'
formatting.methods.express = function(val) {
    return val && val != 0 ? '<i class="fal fa-shipping-fast"></i>' : ''
}
export default formatting