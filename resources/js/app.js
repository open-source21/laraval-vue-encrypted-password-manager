import Vue from "vue";
import App from "./App.vue";
import router from "@/routes";
import store from "@/store/index";
import CONF from '@/_oad_repo/config';
import { utils } from '@/_oad_repo/utils';

Vue.prototype.$utils = utils;
Vue.prototype.$config = CONF;

require ('./_oad_repo/bootstrap/core')
require ('./_oad_repo/bootstrap/axios')
require ('./_oad_repo/bootstrap/form')
require ('./_oad_repo/bootstrap/table')

if (CONF.ROLLBACK_ENV != 'local') {
    require ('./_oad_repo/bootstrap/rollbar');
}

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
