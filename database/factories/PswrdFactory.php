<?php

namespace Database\Factories;

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Factories\Factory;

class PswrdFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'             => $this->faker->word. ' ' . $this->faker->word,
            'username'          => $this->faker->safeEmail,
            'pswrd'             => 'xxxwww555111',
            'url'               => $this->faker->url
        ];
    }
}
