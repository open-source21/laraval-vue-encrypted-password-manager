<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->uuid('hash')->unique();
            $table->primary('hash');
            $table->mediumText('text')->nullable();
            $table->string('assignable_id', 50)->nullable();
            $table->string('assignable_type', 255)->nullable();
            $table->string('assignable_field', 255)->nullable();
            $table->string('assignable_type_slug', 255)->nullable();
            $table->string('user_created')->nullable();
            $table->timestamps();

            // Foreign Keys
            $table->foreign('user_created')->references('hash')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
