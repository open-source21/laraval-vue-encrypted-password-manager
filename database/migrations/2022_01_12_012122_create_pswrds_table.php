<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePswrdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pswrds', function (Blueprint $table) {
            $table->uuid('hash')->unique();
            $table->primary('hash');
            $table->string('title')->nullable();
            $table->string('username')->nullable();
            $table->text('pswrd')->nullable();
            $table->text('url')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pswrds');
    }
}
