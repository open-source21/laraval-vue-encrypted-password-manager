<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePswrdsTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pswrds_tags', function (Blueprint $table) {
            $table->string('pswrds_hash');
            $table->string('tags_hash');
            $table->foreign('pswrds_hash')->references('hash')->on('pswrds');
            $table->foreign('tags_hash')->references('hash')->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pswrds_tags');
    }
}
