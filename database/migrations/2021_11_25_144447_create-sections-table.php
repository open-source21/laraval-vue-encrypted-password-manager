<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable()->unsigned();
            $table->string('text',100)->nullable();
            $table->string('vue_router_id')->nullable();
            $table->string('slug', 100)->nullable();
            $table->string('cssClass')->nullable();
            $table->string('type')->nullable();
            $table->string('access_options')->nullable();
            $table->integer('sort_order')->nullable();

            // Foreign Keys
            $table->foreign('parent_id')->references('id')->on('sections')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
