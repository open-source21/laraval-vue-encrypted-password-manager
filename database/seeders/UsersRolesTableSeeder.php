<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users_roles')->delete();
        DB::table('users_roles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('users_roles')->insert([
            [
                'created_at' => NULL,
                'id' => 1,
                'name' => 'Developer',
                'updated_at' => NULL,
                'user_created' => NULL,
                'user_updated' => NULL,
            ],
            [
                'created_at' => NULL,
                'id' => 2,
                'name' => 'Admin',
                'updated_at' => '2020-05-19 18:27:54',
                'user_created' => NULL,
                'user_updated' => '2',
            ]
        ]);
    }
}
