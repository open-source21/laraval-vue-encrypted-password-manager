<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->delete();
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $users = [
            [
                'id' => 1,
                'hash' => Str::uuid(),
                'name' => 'Andrew',
                'email' => 'andrew@oadsoft.com',
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('xxxwww555111'),
                'roles_id' => 1,
                'sys_access' => true,
                'remember_token' => '',
                'user_updated' => 1,
                'user_created' => 1,
                'last_login' => null,
            ]
        ];

        DB::table('users')->insert($users);

    }
}
