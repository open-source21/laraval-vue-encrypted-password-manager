<?php

namespace Database\Seeders;

use App\_oad_repo\Models\Section;
use App\_oad_repo\Models\UserRolePermission;
use App\Models\VueRouter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('sections')->delete();
        DB::table('sections')->truncate();
        DB::table('users_roles_permissions')->delete();
        DB::table('users_roles_permissions')->truncate();
        DB::table('vue_router')->delete();
        DB::table('vue_router')->truncate();
        DB::table('sections_users_types')->delete();
        DB::table('sections_users_types')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $sections = [
            [
                'access_options' => 'view',
                'cssClass' => 'fal fa-chart-bar',
                'slug' => 'dashboard',
                'text' => 'Dashboard',
                'type' => 'menu',
                'children' => [],
                'roles' => [1, 2],
                'componentPath' => '/pages/dashboard/Dashboard',
                'default' => 1,
                'name' => 'dashboard',
                'path' => '/dashboard',
                'user_types' => ['employee']
            ],
            [
                'access_options' => 'none,view',
                'cssClass' => 'fal fa-cogs',
                'slug' => NULL,
                'text' => 'Admin',
                'type' => 'menu',
                'componentPath' => '#',
                'default' => NULL,
                'name' => 'pathThrough',
                'path' => '/admin',
                'user_types' => ['employee'],
                'children' => [
                    [
                        'access_options' => 'none,view,full',
                        'cssClass' => '',
                        'slug' => 'users',
                        'text' => 'Employees',
                        'type' => 'menu',
                        'roles' => [1, 2],
                        'componentPath' => '/pages/users/Users',
                        'default' => NULL,
                        'name' => 'users',
                        'path' => '/admin/users',
                        'user_types' => ['employee'],
                    ],
                    [
                        'access_options' => 'none,view,full',
                        'cssClass' => NULL,
                        'slug' => 'user_roles',
                        'text' => 'Permissions',
                        'type' => 'menu',
                        'roles' => [1, 2],
                        'componentPath' => '/pages/users/UserRoles',
                        'default' => NULL,
                        'id' => 2,
                        'name' => 'user-roles',
                        'path' => '/admin/user_roles',
                        'user_types' => ['employee'],
                    ]
                ],
                'roles' => [1, 2],
            ],
        ];

        $permissions = [
            [
                'access_options' => 'true,false',
                'cssClass' => '',
                'slug' => 'delete_files',
                'text' => 'Delete Files',
                'type' => 'permission',
                'roles' => [1, 2],
                'user_types' => ['employee'],
            ],
            [
                'access_options' => 'true,false',
                'cssClass' => '',
                'slug' => 'super_admin',
                'text' => 'Super Admin',
                'type' => 'permission',
                'roles' => [1, 2],
                'user_types' => ['employee'],
            ]
        ];

        $data = array_merge($sections, $permissions);

        foreach($data as $index => $item) {
            $section = $this->add_section($item, $index);
            if (!empty($item['children'])) {
                foreach($item['children'] as $childIndex => $child) {
                    $this->add_section($child, $childIndex, $section);
                }
            }
        }

    }

    /**
     * @param $data
     * @param $ordering
     * @param null $parent
     * @return Section
     */
    public function add_section($data, $ordering, $parent = null)
    {
        $section = new Section();
        if ($parent) {
            $section->parent_id = $parent->id;
        }
        $section->text = $data['text'];
        $section->slug = $data['slug'];
        $section->cssClass = $data['cssClass'];
        $section->type = $data['type'];
        $section->access_options = $data['access_options'];
        $section->sort_order = $ordering + 1;
        $section->save();

        if (!empty($data['name'])) {
            $vue_router = new VueRouter();
            $vue_router->name = $data['name'];
            $vue_router->path = $data['path'];
            $vue_router->componentPath = $data['componentPath'];
            $vue_router->default = $data['default'];
            $vue_router->save();

            $section->vue_router_id = $vue_router->id;
            $section->save();
        }


        $roles = $data['roles'];
        foreach($roles as $role) {
            $permissions = $data['access_options'];
            $permissions = explode(',', $permissions);
            foreach($permissions as $permission) {
                if ($permission !== 'false') {
                    $role_permission = new UserRolePermission();
                    $role_permission->users_roles_id = $role;
                    $role_permission->sections_id = $section->id;
                    $role_permission->permission = $permission;

                    $role_permission->save();
                }
            }
        }

        if (!empty($data['user_types'])) {
            foreach($data['user_types'] as $type) {
                DB::table('sections_users_types')->insert([
                    [
                        'sections_id' => $section->id,
                        'user_type' => $type
                    ]
                ]);
            }
        }

        return $section;
    }
}
