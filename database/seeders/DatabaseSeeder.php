<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersRolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SectionsTableSeeder::class);
        if (file_exists(database_path() . '/PersonalAccessTokensTableSeeder.php')){
            $this->call(PersonalAccessTokensTableSeeder::class);
        }

    }
}
