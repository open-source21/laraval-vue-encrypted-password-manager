# SPA Laravel 8 Vue 2



## Getting started

## Installation
```
mkdir ProjectName
cd ProjectName
git clone --recurse-submodules git@gitlab.com:open-source21/laraval-vue-encrypted-password-manager.git .
```
- open and fill in /oad-conf.json

- if you use docker at this point you need to ssh into the project container /
  if no docker - you can just proceed with the instructions below

```
composer install
bash installer.sh
```

## OAD Commands

Show All Commands
```
php artisan oad help
```

Seeds DB
```
php artisan oad seed
```

Drops All tables & Seeds DB, but keeps the sessions live
```
php artisan oad reseed
```

Drops ALL tables on ALL connections
```
php artisan oad dropTables
```

Generate Env File from oad-conf.json
```
php artisan oad genEnv
```

Create Controller
```
php artisan oad mkController
```

Create Model
```
php artisan oad mkModel
```

Create vue index and form templates as a resource
```
php artisan oad mkVueResource
```

Create Index vue file
```
php artisan oad mkVueIndex
```

Create Form vue file
```
php artisan oad mkVueForm
```

Create migration
```
php artisan oad mkMigration
```

Creates a set of files for full CRUD including: migration, model, controller, vueIndex & vueForm
```
php artisan oad mkCrud
```

Generate seeds for sections,vue_router tables
```
php artisan oad iseedNav
```

Generate seeds for users_roles,users_roles_permissions tables
```
php artisan oad iseedRoles
```
