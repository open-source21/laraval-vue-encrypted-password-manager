<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Helpers\RouteHelper;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$router = (new RouteHelper)
    ->addSingleRoute('dashboard','DashboardController')
    ->addResourceRoute('pswrds','PswrdController')
    ->addResourceRoute('users','Admin\UserController')
    ->addResourceRoute('user_roles','Admin\UserRoleController');


require_once app_path('_oad_repo/routes/api.php');
